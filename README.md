[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/mbmaciel/taxabasal)

# Calculadora de taxa metabolica basal

Web app desenvolvido em NodeJs e express para aprendizado da tecnologia.

## O que é

A taxa metabólica basal ou TMB é a quantidade de energia em quilocalorias (Kcal) que seu corpo gasta para sobreviver, ou seja, realizar todas as atividades metabólicas em repouso durante um dia.

## Para que serve 

Para descobrir a quantidade de calorias que precisa para fazer um dieta para emagrecer ou engordar.